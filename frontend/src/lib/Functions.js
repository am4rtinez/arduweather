// Functions.js
// Contiene funciones varias.
import moment from 'moment'

function checkTime (i){
	if(i<10) {i = "0" + i};
	return i;
}

function convertTime (timestamp) {
	let date = new Date(timestamp * 1000);
	let hours = checkTime(date.getHours());
	let minutes = checkTime(date.getMinutes());
	let seconds = checkTime(date.getSeconds());

	// Will display time in 10:30:23 format
	let formattedTime = `${hours}:${minutes}:${seconds}`;
	return formattedTime;
}

function convertDate (timestamp) {
	return moment(timestamp).format("DD/MM/YY HH:mm")
}

function convertUnixDate (timestamp) {
	return moment(timestamp, "X").format("DD/MM/YY HH:mm")
}

function getMaxTemp(array, propName) {
	let max = 0;
	// let maxItem = null;
	for (let item of array) {
		if (item[propName] > max ){
			max = item[propName]
		}
	}
	return max;
}

function getMinTemp(array, propName) {
	let min = 20;  //Media de temperatura ambiente
	// let maxItem = null;
	for (let item of array) {
		if (item[propName] < min ){
			min = item[propName]
		}
	}
	return min;
}

export default {
	checkTime,
	convertTime,
	convertDate,
	convertUnixDate,
	getMaxTemp,
	getMinTemp
}