// API.js
// Contiene funciones varias.

import axios from "axios"

const API_KEY = import.meta.env.VITE_API_OWM_KEY
const CITY = 'Palma'
const WEATHER_URL = `https://api.openweathermap.org/data/2.5/weather?q=${CITY}&appid=${API_KEY}&lang=es&units=metric`
const FORECAST_URL = `https://api.openweathermap.org/data/2.5/forecast?q=${CITY}&appid=${API_KEY}&units=metric`

const server = 'http://localhost:3000/api'

async function getWeather() {
	const response = await fetch(WEATHER_URL)
	return await response.json()

	// const response = await axios.get(WEATHER_URL)
	// return response
}

async function getForecast() {
	const response = await axios.get(FORECAST_URL)
	return response
}

async function getReadingData(station_id, dimension) {
	const response = await axios.get(`/api/readings/${station_id}/${dimension}`)
	return response
}

async function getLatestReadings(station_id) {
	const response = await axios.get(`/api/readings/latest/${station_id}`)
	return response
}

async function getReadingsDimensionByDates(station_id, dimension, start_date, end_date){
	const response = await axios.get(`/api/readings/${station_id}/${dimension}/${start_date}/${end_date}`)
	return response
}

async function getForecastOWM(){
	const response = await axios.get(FORECAST_URL)
	return response
}

export default {
	getWeather,
	getForecast,
	getReadingData,
	getLatestReadings,
	getReadingsDimensionByDates
}