import Map from 'ol/Map'
import View from 'ol/View'

import { useGeographic } from 'ol/proj';

import { layerSwitcher, zoomslider, rotateControl, fullScreen, scaleLine } from './controls'
import { carto_base, maptiler_base, mapbox_base, thunderforest_base, ign,	layers, owm_layers } from './groups'

import { getIGNLayers } from './layers/features/ign'
import { getStations } from './layers/features/mixed'

//long. latitude.
export async function initMap(){
	useGeographic()
	const baleares = [2.73666372, 39.4]

	await getStations(layers)
	getIGNLayers(ign)

	let map = new Map({
		target: 'map',
		view: new View({
			center: baleares,
			zoom: 9,
			rotation: 0
		}),
		layers: [
			carto_base,
			maptiler_base,
			mapbox_base,
			thunderforest_base,
			ign,
			layers,
			owm_layers
		],
	})

	map.addControl(layerSwitcher)
	map.addControl(zoomslider)
	map.addControl(rotateControl)
	map.addControl(fullScreen)
	map.addControl(scaleLine)

	map.on('click', function(evt){
		map.updateSize();
	});

	// setInterval( function() { map.updateSize()}, 200);
}
