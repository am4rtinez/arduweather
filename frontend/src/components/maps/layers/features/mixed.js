import axios from 'axios';
import OSM from 'ol/source/OSM';
import VectorSource from 'ol/source/Vector';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import Feature from 'ol/Feature';
import { Point } from 'ol/geom';
import { Icon, Style, Circle, Fill, Stroke} from 'ol/style';

export const openseamap = new TileLayer({
	title: 'OpenSeaMap',
	visible: false,
  source: new OSM({
    attributions: [
      ' © <a href="https://www.openseamap.org/">OpenSeaMap</a>',
    ],
    opaque: false,
    url: 'https://tiles.openseamap.org/seamark/{z}/{x}/{y}.png',
  }),
});

export async function getStations(layerGroup) {

	// const server = 'http://localhost:3000/api'

	try {
		const response = await axios.get(`/api/stations`)
		let icon_features = []
		for (let station of response.data){
			let iconFeature = new Feature({
				geometry: new Point([station.longitude, station.latitude]), //This marker will not move.
				name: 'Somewhere',
			});
			icon_features.push(iconFeature)
		}
		const vectorLayer =	new VectorLayer({
			title: 'Stations',
			visible: true,
			source: new VectorSource({
				features: icon_features
			}),
			style: new Style({
				image: new Icon({
					anchor: [15, 5],
					anchorXUnits: 'pixels',
					anchorYUnits: 'pixels',
					src: '/wind-gauge-24-black.png'
				})
				// image: new Circle({
				// 	radius: 7,
				// 	stroke: new Stroke({
				// 		color: '#33cc33',
				// 		width: 2,
				// 	}),
				// 	fill: new Fill({
				// 		color: '#ffcc33',
				// 	})
				// })
			})
		})
		//TODO: Station.png <a href="https://www.flaticon.com/free-icons/station" title="station icons">Station icons created by Flat Icons - Flaticon</a>
		//TODO: weather-forecast.png <a href="https://www.flaticon.com/free-icons/meteorology" title="meteorology icons">Meteorology icons created by IwitoStudio - Flaticon</a>
		//TODO: meteorology.png <a href="https://www.flaticon.com/free-icons/climate" title="climate icons">Climate icons created by Freepik - Flaticon</a>
		//TODO: anemometer.png <a href="https://www.flaticon.com/free-icons/meteorology" title="meteorology icons">Meteorology icons created by Eucalyp - Flaticon</a>
		//TODO: weather.png <a href="https://www.flaticon.com/free-icons/weather" title="weather icons">Weather icons created by Freepik - Flaticon</a>
		//<a target="_blank" href="https://icons8.com/icon/102917/wind-gauge">Wind Gauge</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
		layerGroup.getLayers().push(vectorLayer)
	} catch (err) {
		//Do nothing
	}
}