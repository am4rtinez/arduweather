import { ZoomSlider, ScaleLine, FullScreen, Rotate } from 'ol/control';
import LayerSwitcher from 'ol-layerswitcher';

export const layerSwitcher = new LayerSwitcher({
	reverse: false,
	groupSelectStyle: 'none',
})

export const rotateControl = new Rotate({
	autoHide: false,
	className: 'rotation'
})

export const fullScreen = new FullScreen()

export const zoomslider = new ZoomSlider()

export const scaleLine = new ScaleLine({ 
	units: 'metric',
	bar: true
})