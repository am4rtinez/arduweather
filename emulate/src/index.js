import axios from 'axios'
import moment from 'moment'
import { API_OWM_KEY } from './config/config.js'

let minutes = 10
let interval = minutes * 60 * 1000

const lat = 39.56939
const long = 2.65024
const lat2 = 39.7188
const long2 = 2.91554

function main(){
	console.log("------------------------------")
	console.log("| Welcome to Virtual ESP0001 |")
	console.log("------------------------------")
	console.log("")
	console.log("Iniciando consulta: " + moment(new Date).format("DD/MM/YY HH:mm"))
	getOWData()
	getOWData2()
	
	setInterval(function() {
		console.log("Iniciando consulta: " + moment(new Date).format("DD/MM/YY HH:mm"))
		getOWData()
		getOWData2()
	}, interval);
	
}

async function insertData(temp, hum, pres, station_id){
	axios.post(`http://localhost:3000/api/readings/${station_id}`, {
		"temperature": temp,
		"temp_unit_id": 1,
		"humidity": hum,
		"hum_unit_id": 4,
		"pressure": pres,
		"pres_unit_id": 5
	})
	.then((response) => {
		console.log(response.data);
	}, (error) => {
		console.log(error);
	});
}

async function getOWData(){
	const url = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=${API_OWM_KEY}&units=metric`
	const response = await axios.get(url)
	console.log("Temperature: " + response.data.main.temp + ' °C')
	console.log("Humidity: " + response.data.main.humidity + ' %')
	console.log("Pressure: " + response.data.main.pressure + ' hPa')
	console.log("Fecha actualización: " + moment(response.data.dt, 'X').format("DD/MM/YY HH:mm"))
	console.log(" ")
	insertData(response.data.main.temp, response.data.main.humidity, response.data.main.pressure, 'ESP0001')
}
async function getOWData2(){
	const url = `https://api.openweathermap.org/data/2.5/weather?lat=${lat2}&lon=${long2}&appid=${API_OWM_KEY}&units=metric`
	const response = await axios.get(url)
	console.log("Temperature: " + response.data.main.temp + ' °C')
	console.log("Humidity: " + response.data.main.humidity + ' %')
	console.log("Pressure: " + response.data.main.pressure + ' hPa')
	console.log("Fecha actualización: " + moment(response.data.dt, 'X').format("DD/MM/YY HH:mm"))
	console.log(" ")
	insertData(response.data.main.temp, response.data.main.humidity, response.data.main.pressure, 'ESP0002')
}

main()