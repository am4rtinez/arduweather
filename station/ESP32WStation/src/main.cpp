#include <Arduino.h>
#include <Wire.h>
#include <WiFi.h>
#include <Secrets.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_I2CDevice.h>
#include <SPI.h>
#include <Adafruit_BMP280.h>
#include <DHT.h>
#include <HTTPClient.h>

#define WIFI_SSID W_SSID
#define WIFI_PASS W_PASS
#define SERVERNAME SERVER
#define STATION STATION_ID

#ifndef LED_BUILTIN
	#define LED_BUILTIN 2
#endif

#define I2C_SDA 33
#define I2C_SCL 36
#define BME280_ADDRESS (0x76)	// Primary I2C Address
#define BME280_ID (0x58) 			// Primary I2C ID
#define DHTTYPE DHT22   			// Define the DHT sensor type DHT22
#define DHTPIN 23     				// Define pin GIOP23 (37) from ESP32 to conect DHT22 Sensor.

#define SEALEVELPRESSURE_HPA (1019.66) // Default sea level pressure.

Adafruit_BMP280 bmp; // I2C
DHT dht(DHTPIN, DHTTYPE);

const String url = SERVERNAME + String("/") + STATION;

const int TEMP_UNIT_ID = 1;
const int HUM_UNIT_ID = 4;
const int PRES_UNIT_ID = 5;
const int ALT_UNIT_ID = 1;

// unsigned long delayTime;
double temp = 0.0;
double temp280 = 0;
double hum = 0.0;
double pres = 0.0;
double alt = 0.0;
bool Reset = true;

// The following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastTime = 0;
// Timer set to 10 minutes (600000)
unsigned long timerDelay = 600000;
// Set timer to 5 seconds (5000)
// unsigned long timerDelay = 5000;

void WiFiStationConnected(WiFiEvent_t event, WiFiEventInfo_t info){
	digitalWrite(LED_BUILTIN, HIGH);
  Serial.println("Connected to " + WiFi.SSID() + " successfully!");
}

void WiFiGotIP(WiFiEvent_t event, WiFiEventInfo_t info){
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
	Serial.println(WiFi.localIP());
	Serial.print("RRSI: ");
  Serial.println(WiFi.RSSI());
}

void WiFiStationDisconnected(WiFiEvent_t event, WiFiEventInfo_t info){
  Serial.println("Disconnected from WiFi access point");
  Serial.print("WiFi lost connection. Reason: ");
	digitalWrite(LED_BUILTIN, LOW);
  Serial.println("Trying to Reconnect");
}

void getReadings() {
	temp = dht.readTemperature();
	temp280 = bmp.readTemperature();
	hum = dht.readHumidity();
	pres = bmp.readPressure()/100;
	alt = bmp.readAltitude(SEALEVELPRESSURE_HPA);
}

void printValues() {
	Serial.println();
	Serial.println("------------------- READINGS -----------------------");
	Serial.print(F("Temperature BMP280 = "));
	Serial.print(temp280);
	Serial.println(" °C");

	Serial.print(F("Pressure = "));
	Serial.print(pres); 									//Displays the Pressure in hPa.
	Serial.println(" hPa");

	Serial.print(F("Approx altitude = "));
	Serial.print(alt);		//The "1019.66" is the pressure(hPa) at sea level in day in your region
	Serial.println("m");

	Serial.print(F("Temperature DHT22 = "));
	Serial.print(temp);
	Serial.println(" °C");

	Serial.print(F("Humedad DHT22 = "));
	Serial.print(hum);
	Serial.println(" %");
	Serial.println("----------------------------------------------------");

	Serial.println();
}

/**
 * Setup Method
*/
void setup() {
	Serial.begin(115200);						// Trabaja a 115200
	pinMode(LED_BUILTIN, OUTPUT);		// Configura el led como salida.

	Serial.println();
	Serial.println("[ Starting ESP32 Weather - OK ]");
	Serial.println("----------------------------------------------------");
	Serial.println();

	if (!bmp.begin(BME280_ADDRESS,BME280_ID)) {
    Serial.println(F("Could not find a valid BMP280 sensor, check wiring!"));
    while (1);
  }
	dht.begin();

	WiFi.disconnect(true);
	delay(1000);

	WiFi.onEvent(WiFiStationConnected,ARDUINO_EVENT_WIFI_STA_CONNECTED);
	WiFi.onEvent(WiFiGotIP, ARDUINO_EVENT_WIFI_STA_GOT_IP); 
	WiFi.onEvent(WiFiStationDisconnected, ARDUINO_EVENT_WIFI_STA_DISCONNECTED); 
	WiFi.begin(WIFI_SSID, WIFI_PASS);

	Serial.println("Wait for WiFi...");
}

void postRequest(){
	HTTPClient http;
	WiFiClient client;
	// Your Domain name with URL path or IP address with path
	if (http.begin(client, url)){
		// If you need an HTTP request with a content type: application/json, use the following:
		http.addHeader("Content-Type", "application/json");
		
		String httpRequestJSON = "{\"temperature\":" + String(temp) 
			+ ", \"temp_unit_id\":" + TEMP_UNIT_ID 
			+ ", \"humidity\":" + hum 
			+ ", \"hum_unit_id\":" + HUM_UNIT_ID 
			+ ", \"pressure\":" + pres 
			+ ", \"pres_unit_id\":" + PRES_UNIT_ID 
			+ "}";
		
		Serial.println("Sending POST HTTPRequest..");

		int httpResponseCode = http.POST(httpRequestJSON);

		String payload = "--"; 

		if (httpResponseCode > 0) {
			Serial.print("HTTP Response code: ");
			Serial.println(httpResponseCode);
			payload = http.getString();
		}
		else {
			Serial.print("Error code: ");
			Serial.println(httpResponseCode);
		}
		// Free resources
		http.end();

	} else {
		Serial.println("[HTTP] Unable to connect");
	}

}

void loop() {
	if (Reset == true) {
		Reset = false;
		delay(10000);
		if(WiFi.status() == WL_CONNECTED){
			getReadings();
			printValues();
			postRequest();
		} else {
			Serial.println("WiFi Disconnected");
		}
	}else{
		//Send an HTTP POST request every 10 minutes
		if ((millis() - lastTime) > timerDelay) {
			//Check WiFi connection status
			if(WiFi.status() == WL_CONNECTED){
				getReadings();
				printValues();
				postRequest();
			} else {
				Serial.println("WiFi Disconnected");
			}
			lastTime = millis();
		}
	}
}

