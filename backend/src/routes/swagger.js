import { Router } from 'express'
// import swaggerJSDoc from 'swagger-jsdoc'
import swaggerUi from 'swagger-ui-express'
import swagger from '../docs/swagger.js'

const router = Router()

// 	apis: ['./src/routes/*.js']

router.use('/docs', swaggerUi.serve);
router.get('/docs', swaggerUi.setup(swagger))

export default router