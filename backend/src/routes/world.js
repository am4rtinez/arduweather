import { Router } from "express";
import * as worldController from '../controllers/world.js'

const router = Router()

router.get('/regions', worldController.getRegions)
router.get('/subregions/:region', worldController.getSubregions)
//TODO: States endpoints
router.get('/countries', worldController.getCountries)
router.get('/countries/:region', worldController.getCountriesRegion)

export default router