import { Router } from 'express'
import { getUser, postUser } from '../controllers/users.js'
import validateUsers from '../validators/users.js'

const router = Router()

router.post('/users', validateUsers, postUser)
router.get('/users/:username', getUser)
// router.put('/users/:id', validateUsers, )
// router.delete('/users/:username', )

export default router