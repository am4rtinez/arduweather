import { Router } from 'express'
import * as stationsController from '../controllers/stations.js'
import validateStation from '../validators/stations.js'

const router = Router()

router.get('/stations', stationsController.getStations)
router.post('/stations', validateStation, stationsController.postStations)
router.get('/stations/:id', stationsController.getStationId)
router.put('/stations/:id', validateStation, stationsController.putStations)
router.delete('/stations/:id', stationsController.deleteStations)

export default router