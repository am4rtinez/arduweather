import { Router } from 'express'
import swaggerRoute from './swagger.js'
import stationsRoute from './stations.js'
import sensorsRoute from './sensors.js'
import usersRoute from './users.js'
import unitsRoute from './units.js'
import worldRoute from './world.js'
import readingsRoute from './readings.js'

const router = Router()

router.get('/', (req, res) => {
	res.send('Hola Mundo!')
})

router.use(stationsRoute)
router.use(sensorsRoute)
router.use(usersRoute)
router.use(unitsRoute)
router.use(worldRoute)
router.use(readingsRoute)
router.use(swaggerRoute)

export default router