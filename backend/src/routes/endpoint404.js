const endpoint404 = (req, res) => {
	res.status(404).send({ message: 'Unknown endpoint' })
}

export default endpoint404