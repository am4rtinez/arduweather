import { check } from 'express-validator'
import { validateResult } from '../helpers/validateHelper.js'

const validateReadings = [
	check('temperature')
		.exists()
		.isDecimal({force_decimal: false, decimal_digits: '0,3'})
		.not()
		.isEmpty(),
	check('temp_unit_id')
		.exists()
		.isInt()
		.not()
		.isEmpty(),
	check('humidity')
		.exists()
		.isDecimal({force_decimal: false, decimal_digits: '0,3'})
		.not()
		.isEmpty(),
	check('hum_unit_id')
		.exists()
		.isInt()
		.not()
		.isEmpty(),
	check('pressure')
		.exists()
		.isDecimal({force_decimal: false, decimal_digits: '0,3'})
		.not()
		.isEmpty(),
	check('pres_unit_id')
		.exists()
		.isInt()
		.not()
		.isEmpty(),
	(req, res, next) => {
		validateResult(req, res, next)
	}
]

export default validateReadings;