import { check } from 'express-validator'
import { validateResult } from '../helpers/validateHelper.js'

const validateUsers = [
	check('username')
		.exists()
		.isAlphanumeric()
		.isLength({min:5, max:15})
		.not()
		.isEmpty(),
	check('name')
		.exists()
		.not()
		.isEmpty(),
	check('lastname')
		.exists()
		.not()
		.isEmpty(),
	check('email')
		.exists()
		.isEmail(),
	(req, res, next) => {
		validateResult(req, res, next)
	}
]

export default validateUsers;