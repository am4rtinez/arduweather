import express from 'express';
import cors from 'cors'
import morgan from 'morgan'
import { PORT } from './config/config.js'
import routes from './routes/index.js'
import apiRoutes from './routes/api.js'
import endpoint404 from './routes/endpoint404.js';

const app = express()

// Settings
app.set('port', PORT)

//Middlewares
app.use(cors())
app.use(express.json())
app.use(express.text())
app.use(express.urlencoded({ extended: true}))
app.use(morgan('dev'))

// Routes
app.use('/', routes)					// Rutas del site apuntaran a la pagina con vue.
app.use('/api', apiRoutes)		// Rutas de la api
app.use(express.static('dist'))

app.use(endpoint404)

// Server initialization
app.listen(app.set('port'), '0.0.0.0', () => {
	console.log(`Servidor ejecutandose en http://localhost:${app.set('port')}`)
})