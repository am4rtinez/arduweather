export default {
	tags:[
		{
			name: "Users",
			description: "The users managin API",
		},
		{
			name: "Stations",
			description: "The stations managin API",
		},
		{
			name: "Sensors",
			description: "The sensors managin API",
		},
		{
			name: "Units",
			description: "The units managin API",
		},
		{
			name: "Dimension",
			description: "The dimensions managin API",
		},
		{
			name: "World",
			description: "The world managin API",
		}
	]
}