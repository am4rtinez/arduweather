import { config } from 'dotenv'
config()
const PORT = process.env.PORT || 3000

export default {
  servers: [
    {
			url: `http://localhost:${PORT}`, // url
			description: "Local server", // name
		},
    {
			url: `http://lilith:${PORT}`, // url
			description: "Dev lilith server", // name
		},
    {
			url: `http://zeus:${PORT}`, // url
			description: "Dev Zeus server", // name
		},
    {
			url: `http://sirius:${PORT}`, // url
			description: "Dev Zeus server", // name
		},
    {
			url: `http://esp32weather.es`, // url
			description: "Production server", // name
		}
  ],
};