export default {
	components: {
		schemas: {
			User:{
				type: "object",
				properties:{
					username:{
						type: "string",
					},
					name:{
						type: "string",
					},
					lastname:{
						type: "string",
					},
					email:{
						type: "string"
					}
				}
			},
			Dimension:{
				type: "object",
				properties: {
					id:{
						type: "integer",
						format: "int64",
						example: 1
					},
					dimension: {
						type: "string",
						example: "temperature"
					}
				}
			},
			Station:{
				type: "object",
				properties:{
					id:{
						type: "string",
					},
					name:{
						type: "string",
					},
					description:{
						type: "string",
					},
					username:{
						type: "string"
					},
					city:{
						type: "integer",
						format: "int64"
					},
					latitude:{
						type: "double"
					},
					longitude:{
						type: "double"
					}
				}
			},
			Region: {
				type: "object",
				properties:{
					id:{
						type: "integer",
						format: "int64",
					},
					name:{
						type: "string",
					}
				}
			},
			Subregion: {
				type: "object",
				properties:{
					id:{
						type: "integer",
						format: "int64",
					},
					name:{
						type: "string"
					}
				}
			},
			Country: {
				type: "object",
				properties:{
					id:{
						type: "integer",
						format: "int64",
					}, 
					name:{
						type: "string"
					}, 
					iso2:{
						type: "string",
						format: "alpha-2"
					}, 
					iso3:{
						type: "string",
						format: "alpha-3"
					}, 
					region:{
						type: "string"
					}, 
					subregion:{
						type: "string"
					}, 
					latitude:{
						type: "number",
						format: "float"
					}, 
					longitude:{
						type: "number",
						format: "float"
					}, 
					emoji:{
						type: "string"
					}, 
					emojiU:{
						type: "string"
					}
				}
			},
			State: {
				type: "object",
				properties:{
					id:{
						type: "integer",
						format: "int64",
					}, 
					name:{
						type: "string"
					}, 
					iso2:{
						type: "string",
						format: "alpha-2"
					}, 
					iso3:{
						type: "string",
						format: "alpha-3"
					}, 
					region:{
						type: "string"
					}, 
					subregion:{
						type: "string"
					}, 
					latitude:{
						type: "number",
						format: "float"
					}, 
					longitude:{
						type: "number",
						format: "float"
					}, 
					emoji:{
						type: "string"
					}, 
					emojiU:{
						type: "string"
					}
				}
			},
			City: {
				type: "object",
				properties:{
					id:{
						type: "integer",
						format: "int64",
					}, 
					name:{
						type: "string"
					}, 
					iso2:{
						type: "string",
						format: "alpha-2"
					}, 
					iso3:{
						type: "string",
						format: "alpha-3"
					}, 
					region:{
						type: "string"
					}, 
					subregion:{
						type: "string"
					}, 
					latitude:{
						type: "number",
						format: "float"
					}, 
					longitude:{
						type: "number",
						format: "float"
					}, 
					emoji:{
						type: "string"
					}, 
					emojiU:{
						type: "string"
					}
				}
			},
			Sensor:{
				type: "object",
				properties:{
					id:{
						type: "integer",
						format: "int64"
					},
					station_id:{
						type: "string"
					},
					name:{
						type: "string",
					},
					description:{
						type: "string",
					},
					manufacturer:{
						type: "string",
					},
					sensor_type:{
						type: "string",
					}
				}
			},
			Unit:{
				type: "object",
				properties:{
					id:{
						type: "integer",
						format: "int64"
					},
					dimension_id:{
						type: "integer",
						format: "int64"
					},
					dimension:{
						type: "string"
					},
					unit:{
						type: "string"
					},
					representation:{
						type: "string"
					}
				}
			},
			StationReadings:{
				type: "array",
				items : {
					type: "object",
					properties: {
						data: {
							type: "array",
							items: {
								type: "object",
								properties: {
									station_id: {
										type: "string"
									},
									datetime: {
										type: "string"
									},
									readings: {
										type: "array",
										items: {
											type: "object",
											properties: {
												dimension: {
													type: "number"
												},
												value: {
													type: "number"
												},
												representation: {
													type: "string"
												},
												unit: {
													type: "string"
												},
												unit_id: {
													type: "integer"
												},
											}
										}
									},
								}
							}
						}
					}
				}
			},
			DimensionReadings:{
				type: "array",
				items : {
					type: "object",
					properties: {
						datetime: {
							type: "string",
							format: "date-time"
						},
						dimension: {
							type: "number"
						},
						representation: {
							type: "string"
						},
						unit: {
							type: "string"
						},
						unit_id: {
							type: "integer"
						},
					}
				}
			},
			Readings: {
				type: "object",
				properties: {
					"datetime": {
						type: "string",
						format: "date-time"
					},
					"temperature": {
						type: "number",
						format: "double"
					},
					"temp_unit_id": {
						type: "integer",
						format: "int64"
					},
					"humidity": {
						type: "number",
						format: "double"
					},
					"hum_unit_id": {
						type: "integer",
						format: "int64"
					},
					"pressure": {
						type: "number",
						format: "double"
					},
					"pres_unit_id": {
						type: "integer",
						format: "int64"
					}
				}
			},
			ServerError: {
				type: "object",
				properties:{
					message: {
						type: "string",
						example: "Something goes wrong."
					}
				}
			},
			UnknownEndpoint: {
				type: "object",
				properties:{
					message: {
						type: "string",
						example: "Unknown endpoint."
					}
				}
			}
		}
	}
}