const getStationReadingsQuery = (station_id) => {
	return `select json_arrayagg(
						json_object(
							"station_id", t.station_id, 
							"datetime", t.datetime, 
							"readings", t.js
						)
					)as "data"
					from (
						select 
							r.station_id, 
							r.datetime, 
							json_arrayagg(
								json_object(
									"dimension", ud.dimension, 
									"value", r.value, 
									"representation", u.representation, 
									"unit", u.unit, 
									"unit_id", r.unit_id
								)
							) js 
						from readings r 
						left join units u on u.id = r.unit_id 
						left join unit_dimension ud on ud.id = u.dimension_id
						WHERE r.station_id = '${station_id}'
						group by r.station_id, r.datetime
						order by r.datetime DESC, r.unit_id ASC
						LIMIT 20
					) t;`
}

const getStationReadingsByDatesQuery = (station_id, start_date, end_date) => {
	return `select json_arrayagg(
						json_object(
							"station_id", t.station_id, 
							"datetime", t.datetime, 
							"readings", t.js
						)
					)as data
					from (
						select 
							r.station_id, 
							r.datetime, 
							json_arrayagg(
								json_object(
									"dimension", ud.dimension, 
									"value", r.value, 
									"representation", u.representation, 
									"unit", u.unit, "unit_id", r.unit_id
								)
							) js 
						from readings r 
						left join units u on u.id = r.unit_id 
						left join unit_dimension ud on ud.id = u.dimension_id
						WHERE r.station_id = '${station_id}'
						AND r.datetime BETWEEN '${start_date}' AND '${end_date} 23:59:59'
						group by r.station_id, r.datetime
						order by r.datetime DESC, r.unit_id ASC
					) t;`
}

const getStationLastReadingsQuery = (station_id) => {
	return `select 
						json_object(
							"station_id", t.station_id, 
							"datetime", t.datetime, 
							"readings", t.js
						) "data"
						from (
						select 
							r.station_id, 
							r.datetime, 
							json_arrayagg(
								json_object(
									"dimension", ud.dimension, 
									"value", r.value, 
									"representation", u.representation, 
									"unit", u.unit, "unit_id", r.unit_id
								)
							) js 
						from readings r 
						left join units u on u.id = r.unit_id 
						left join unit_dimension ud on ud.id = u.dimension_id
						WHERE r.station_id = '${station_id}'
						group by r.station_id, r.datetime
						order by r.datetime DESC, r.unit_id ASC
						LIMIT 1
					) t;`
}

const getStationsReadingsByDimensionQuery = (station_id, dimension) => {
	return `SELECT * FROM (
						SELECT r.datetime, r.value as '${dimension}', u.representation, u.unit, u.id as unit_id 
						FROM readings r
						inner join units u on (u.id = r.unit_id)
						inner join unit_dimension ud on (ud.id = u.dimension_id)
						WHERE r.station_id = '${station_id}' 
						AND ud.dimension = '${dimension}'
						ORDER BY r.datetime DESC LIMIT 20)
					sub order by datetime ASC;`
}

const getStationReadingsDimensionByDatesQuery = (station_id, dimension, start_date, end_date) => {
	return `SELECT * FROM (
		SELECT r.datetime, r.value as '${dimension}', u.representation, u.unit  
		FROM readings r
		inner join units u on (u.id = r.unit_id)
		inner join unit_dimension ud on (ud.id = u.dimension_id)
		WHERE r.station_id = '${station_id}' 
		AND ud.dimension = '${dimension}'
		AND r.datetime BETWEEN '${start_date}' AND '${end_date} 23:59:59'  
		ORDER BY r.datetime DESC)
	sub order by datetime ASC;`
}

export {
	getStationReadingsQuery,
	getStationReadingsByDatesQuery,
	getStationLastReadingsQuery,
	getStationsReadingsByDimensionQuery,
	getStationReadingsDimensionByDatesQuery
}