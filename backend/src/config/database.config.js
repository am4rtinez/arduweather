import { config } from 'dotenv'

config()
// DB configuration
const DBHOST = process.env.DB_HOST
const DBPORT = process.env.DB_PORT
const DBNAME = process.env.DB_NAME
const DBUSER = process.env.DB_USER
const DBPASS = process.env.DB_PASSWORD
const DBDIALECT = process.env.DB_DIALECT

export { DBHOST, DBPORT, DBNAME, DBUSER, DBPASS, DBDIALECT };