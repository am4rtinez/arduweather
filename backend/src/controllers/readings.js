import pool from "../helpers/database.js"
import { getStationReadingsQuery, getStationReadingsByDatesQuery, getStationLastReadingsQuery, getStationsReadingsByDimensionQuery, getStationReadingsDimensionByDatesQuery } from "../config/database.queries.js"

const getStationReadings = async (req, res, next) => {
		try {
		const sql = getStationReadingsQuery(req.params.station_id)
		const result = await pool.query(sql)
		if (result.length > 0) {
			res.status(200).json(result)
		} else {
			res.status(404).send({ message : 'No Data Found' }).end()
		}
	} catch (err){
		printError(err)
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

const getStationLastReadings = async (req, res, next) => {
	try {
		const sql = getStationLastReadingsQuery(req.params.station_id)
		const result = await pool.query(sql)
		if (result.length > 0) {
			res.status(200).json(result)
		} else {
			res.status(404).send({ message : 'No Data Found' }).end()
		}
	} catch (err){
		printError(err)
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

const getStationReadingsByDates = async (req, res, next) => {
	try {
		const sql = getStationReadingsByDatesQuery(req.params.station_id, req.params.start_date, req.params.end_date)
		const result = await pool.query(sql)
		console.log(result)
		if (result.length > 0 && result[0].data != null) {
			res.status(200).json(result)
		} else {
			res.status(404).send({ message : 'No data found' }).end()
		}
	} catch (err){
		printError(err)
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

const getStationReadingsByDimension = async (req, res, next) => {
	try{
		const sql = getStationsReadingsByDimensionQuery(req.params.station_id, req.params.dimension)
		const result = await pool.query(sql)
		if (result.length > 0) {
			res.status(200).json(result)
		} else {
			res.status(404).send({ message : 'No data found' }).end()
		}
	} catch (err){
		printError(err)
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

const getStationReadingsByDimensionByDates = async (req, res, next) => {
	try{
		const sql = getStationReadingsDimensionByDatesQuery(req.params.station_id, req.params.dimension, req.params.start_date, req.params.end_date)
		const result = await pool.query(sql)
		if (result.length > 0 && result[0].data != null) {
			res.status(200).json(result)
		} else {
			res.status(404).send({ message : 'No data found' }).end()
		}
	} catch (err){
		printError(err)
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

const postStationReadings = async (req, res, next) => {
	let sql = 'INSERT INTO readings (station_id, unit_id, value) VALUES '
	sql = sql + `('${req.params.station_id}', ${req.body.temp_unit_id} ,${req.body.temperature}),
	('${req.params.station_id}', ${req.body.hum_unit_id} ,${req.body.humidity}),
	('${req.params.station_id}', ${req.body.pres_unit_id} ,${req.body.pressure});`
	try {
		const result = await pool.query(sql)
		console.log(new Date)
		res.status(201).json({ message: 'Data was successfully inserted.' });
	} catch (err) {
		printError(err)
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

const printError = (err) => {
	console.log({
		message: err.text,
		errno: err.errno,
		code: err.code
	})
}

export { 
	getStationReadings, 
	getStationReadingsByDates,
	getStationReadingsByDimension,
	getStationReadingsByDimensionByDates,
	postStationReadings,
	getStationLastReadings 
}