import pool from "../helpers/database.js"

const getUser = async (req, res, next) => {
	try {
		const sql = `SELECT username, name, lastname, email FROM users WHERE username = '${req.params.username}'`
		const result = await pool.query(sql)
		if (result.length > 0) {
			res.status(200).json(result)
		} else {
			res.status(404).send({ message : 'The user was not found.' }).end()
		}
	} catch (err){
		console.log({
			message: err.text,
			errno: err.errno,
			code: err.code
		})
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

const postUser = async (req, res, next) => {
	try {
		const count = `SELECT count(*) as count FROM users WHERE username = '${req.body.username}'`
		const rc = await pool.query(count)
		// Check if id already exists.
		if (rc[0].count > 0) {
			res.status(209).json({ message: 'This username already exists.' });
		} else {
			const sql = `INSERT INTO users (username, name, lastname, email) 
				VALUES ('${req.body.username}','${req.body.name}','${req.body.lastname}','${req.body.email}')`
			const result = await pool.query(sql)
			res.status(201).json({ message: 'User was successfully created.' });
		}
	} catch (err) {
		console.log({
			message: err.text,
			errno: err.errno,
			code: err.code
		})
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

export { getUser, postUser }